import React, { Component } from 'react';

import {Modal} from './Modal'
import {Button} from './Button'

export class FullPage extends Component {
    constructor(){
        super();
        this.state = {
            firstIsShowing: false,
            secondIsShowing: false
        }
    }
    setFirstShowing = () => {
        this.setState({firstIsShowing: !this.state.firstIsShowing})
 
    }
    setSecondShowing = () => {
        this.setState({secondIsShowing:!this.state.secondIsShowing})
 
    }
    
    render(){
     
    return (
        <div className='main-modal'>
        <div onClick={this.setFirstShowing}>
        {this.state.firstIsShowing ? 
        <Modal
              header="Do you want to delete this file? " 
              closeIcon={true}
              color = "E29484"
              toggle={this.setFirstShowing}
              text="Once you delete this file, it won’t be possible to undo this action. 
              
             Are you sure you want to delete it?"   
               action ={[ <Button 
                        text="Ok" 
                        backgroundColor="#b3382c" 
                        onClick={this.setFirstShowing}/>,
                        <Button 
                        text="Cancel" 
                        backgroundColor="#b3382c" 
                        onClick={this.setFirstShowing}/>]} /> : null}</div>
                                                           

          <div onClick={this.setSecondShowing}>
            {this.state.secondIsShowing ? 
             <Modal
             header="Do you want to confirm this file? " 
             toggle={this.setSecondShowing}
             backgroundColor="#E29484"
             closeIcon={true}
             text="If you confirm this file, all process are accepted. And it won't be possible to undo this process.
              Are you sure you want to confirm this file? "
             action ={[ 
             <Button 
                        text="Confirm" 
                        backgroundColor="#E29484" 
                        onClick={this.setSecondShowing}/>,
            <Button 
                        text="Cancel" 
                        backgroundColor="#E29484" 
                        onClick={this.setSecondShowing}/>]}/>: null }</div>
                 
            
        <Button text="Open first modal " backgroundColor="#99a2ff" onClick={this.setFirstShowing}/>
        <Button text="Open second modal " backgroundColor="#34f4a7" onClick={this.setSecondShowing}/>
            
            
        </div>
    )
}
}   
