import React from 'react';
import './App.css';
import { FullPage } from './components/FullPage';
import './styles.scss';

function App() {
    return ( 
    <div className = "App" >
        <FullPage/>
        </div>
    );
}

export default App;